# chat app

2016-06-06.

This is based on http://bamboolab.eu/blog/implement-a-chat-app-in-rails-4.

Currently, render_sync not working: 

     Could not find generator 'render_sync:install'


So I am just trying to improve the look and feel and learn a bit of rails by playing around with it.


## Learnings

1. Instead of printing user object, I can access the username due to `belongs_to` by doing `@message.user.name`.
2. Use `cycle` to alternate background colors in the message listing.
3. Use `time_ago_in_words(message.created_at)` to print human friendly time.
