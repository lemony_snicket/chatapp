class Message < ActiveRecord::Base
  belongs_to :user
  validates :body, length: { maximum: 500 },
    presence: true
end
